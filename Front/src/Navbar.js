import React from 'react';
import { AppBar , Toolbar, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import { Link } from 'react-router-dom'

const useStyles = makeStyles(() => ({
    typography: {
        fontWeight: 'bold',
        fontFamily: 'Times New Roman',
    },
    icon: {
        paddingBottom: 8,
        fontSize: 48,
    },
    toolbar: {
        paddingLeft: 8,
        paddingRight: 0,
    },
    linkPosition: {
        width: '71%',
        textAlign: 'right',
    },
    link: {
        paddingLeft: '1%',
        paddingRight: '1%',
        textDecoration: 'none',
        color: 'black',
        fontFamily: 'Trebuchet MS'
    }
}))

const Navbar = () => {
    
    const classes = useStyles();

    return ( 
        <AppBar position="static">
            <Toolbar className={classes.toolbar}>
                <Typography variant="h3" gutterBottom className={classes.typography}>
                    Intelligence App
                </Typography>
                <VerifiedUserIcon className={classes.icon}/>
                <Typography className={classes.linkPosition}>
                    <Link to='/' className={classes.link}>Home</Link>
                    |
                    <Link to='/support' className={classes.link}>Support</Link>
                </Typography>
            </Toolbar>
        </AppBar>
    );
}
 
export default Navbar;