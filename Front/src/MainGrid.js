import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Typography, Grid, Divider } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    paper: {
        marginTop: 30,
        background: "#3f51b5",
        borderRadius: 10,
        padding: 16
    },
    typographyVal: {
        fontFamily: 'Comic Sans MS',
        padding: 8,
        whiteSpace: 'pre-line',
    },
    typographyKey: {
        fontWeight: 'bold',
        display: "inline",
        fontSize: '1.2rem',
        fontFamily: 'Comic Sans MS',
    },
    typographyID: {
        marginBottom: 20,
        fontWeight: 'bold',
        fontSize: '1.5rem',
        fontFamily: 'Comic Sans MS',
        color: 'white',
    },
}))

const MainGrid = ({attack}) => {

    const classes = useStyles();
    let attackKeys = Object.keys(attack).filter(key => key !== 'id')

    return (
        <Paper className={classes.paper}>
            <Grid container>
                <Grid item xs={12}>
                    <Typography align='center' className={classes.typographyID}>{attack.id}</Typography>
                </Grid>
                {
                    Object.entries(attack).filter(([key, value]) => key !== 'id').map(([key, value], i) => (
                        <Grid item key={i} xs={12}>
                            <Typography className={classes.typographyKey}>{key}: </Typography>
                            <Typography className={classes.typographyVal}>{value}</Typography>
                            {i !== (attackKeys.length - 1) ? <Divider/> : <></>} 
                        </Grid>
                    ))
                }
            </Grid>
        </Paper>   
    );
}
 
export default MainGrid