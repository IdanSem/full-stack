import React, {useState, useEffect} from 'react';
import MainGrid from './MainGrid'
import InfiniteScroll from 'react-infinite-scroll-component'
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
    grid: {
        margin: 'auto',
        width: '80%',
    },
    typographyEnd: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '1.2rem',
        fontFamily: 'Comic Sans MS',
    },
}))

const AttacksPanel = ({attacks}) => {
    
    const classes = useStyles();
    const [attacksDisplayed, setAttacksDisplayed] = useState(attacks !== null ? attacks.slice(-4):null)
    const numberOfAttacks = 4

    const fetchNext = () => {
        setAttacksDisplayed(attacks.slice(-1 * (attacksDisplayed.length + 
            ((attacksDisplayed.length + 4 > attacks.length)
            ? attacks.length - attacksDisplayed.length 
            : numberOfAttacks)))
        )
    }

    useEffect(() => {
        setAttacksDisplayed(
            (attacks.length < numberOfAttacks) 
            ? attacks
            : attacks.slice(-1 * numberOfAttacks)
        ) 
    }, [attacks])

    return (
        (attacksDisplayed === null) 
        ? <div></div> 
        :
        <InfiniteScroll
        dataLength={attacksDisplayed.length}
        hasMore={attacksDisplayed.length !== attacks.length}
        loader={<Typography className={classes.typographyEnd}>Loading...</Typography>}
        next={fetchNext}
        endMessage={
            <Typography className={classes.typographyEnd}>{attacksDisplayed.length ? "You have reached the end!" : null}</Typography>
        }
        >
            {
                <Grid container className={classes.grid}>
                    {
                        attacksDisplayed.map((attack, i) => (
                            <Grid item xs={12} key={i}>
                                <MainGrid attack={attack}/>
                            </Grid>
                        ))
                    }
                </Grid>
            }
        </InfiniteScroll>
    )
}
 
export default AttacksPanel