import React, {useState, useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AttacksPanel from './AttacksPanel'
import { Typography, TextField, InputAdornment, InputLabel, FormControl, Select } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import Axios from 'axios'

const useStyles = makeStyles(() => ({
    pending: {
      height: 100,
    },
    root: {
      flexGrow: 1,
      marginTop: 20,
      width: '100%'
    },
    typographyTheme: {
      fontWeight: 'bold',
      fontSize: '1.75rem',
      fontFamily: 'Comic Sans MS',
      color: 'white',
    },
    typographySearch: {
      fontWeight: 'bold',
      fontSize: '2.23rem',
      fontFamily: 'Comic Sans MS',
    },
    searchBarContainer: {
      width: '80%',
      margin: 'auto'
    },
    formControl: {
      marginTop: 20,
    },
    optionC: {
      fontFamily: 'Trebuchet MS',
      background: '#222222 !important',
    },
    selectC: {
      fontFamily: 'Trebuchet MS',
      color: 'white',
    },
    icon: {
      fill: 'white',
    },
    inputC: {
      color: 'white'
    }
}));

const AttacksPages = () => {

  const classes = useStyles()
  const [attacks, setAttacks] = useState(null)
  const [keyWords, setKey] = useState('')
  const [isDeleteKey, setIsDelete] = useState(false)
  const [selectedKey, setSelectedKey] = useState('description')
  let attacksFilter = (!attacks) ? null : attacks 

  if(attacksFilter) {
    attacksFilter =
      (isDeleteKey) 
      ? attacks.filter(attack => attack[selectedKey].includes(keyWords))
      : attacksFilter.filter(attack => attack[selectedKey].includes(keyWords))
  }

  useEffect(() => {
    Axios.get('http://localhost:3001/idanApi/getData')
    .then(({data}) => {
        setAttacks(data)
    })
    .catch(err => {
        console.log(`error: ${err}`)
    })
  }, [])

  function changeTextHandle(e) {
    setKey(e.target.value)
  }
  
  function keyHandler(e) {
    let key = e.keyCode || e.charCode
    setIsDelete(key === 8 || key === 46)
  }

  return (
    <div className={classes.root}>
      {
        attacks
        ? 
        (
          <>
            <div className={classes.searchBarContainer}>
              <TextField
                autoComplete='off'
                fullWidth
                id="filled-basic" 
                label="Search: " 
                variant="filled" 
                onChange={changeTextHandle} 
                onKeyDown={keyHandler}
                InputProps={
                {
                  classes: {
                    input: classes.inputC
                  },
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon color="primary"/>
                    </InputAdornment>
                  )
                }
                }
              />
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-By-native-simple">By:</InputLabel>
                <Select
                  className={classes.selectC}
                  native
                  value={selectedKey}
                  onChange={e => {setSelectedKey(e.target.value)}}
                  label="By"
                  inputProps={{
                    name: 'By',
                    id: 'outlined-By-native-simple',
                    classes: { 
                      icon: classes.icon,
                    },
                  }}
                >
                  <>
                    <option value={'description'} className={classes.optionC}>description</option>
                    {attacks !== undefined 
                      ? Object.keys(attacks[0]).filter(key => key !== 'description').map((key, index) => <option value={key} key={index} className={classes.optionC}>{key}</option>) 
                      : <></>
                    }
                  </>
                </Select>
              </FormControl>
              <Typography
              align='left' 
              className={classes.typographyTheme}>
                
                {
                  (keyWords === '' ? attacks.length : attacksFilter.length) + " Attacks Found" + (attacksFilter.length ? ":" : ".")
                } 
              </Typography>
            </div>
            
            <AttacksPanel attacks={keyWords === '' ? attacks: attacksFilter}/>
          </>
        )
        :
        <div className={classes.typographyTheme}>Loading Data...</div>
      }
  </div>
  )
}

export default AttacksPages;