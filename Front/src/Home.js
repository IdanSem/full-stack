import React from 'react'
import ScrollUp from 'react-scroll-up';
import NavigationIcon from '@material-ui/icons/Navigation';
import AttacksPages from './AttacksPages'
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    icon: {
        color: 'white',
        marginRight: 16,
        fontSize: 32,
    },
})) 
  

const Home = () => {

    const classes = useStyles()

    return ( 
        <div>
            <AttacksPages/>
            <ScrollUp showUnder={160}>
            <NavigationIcon className={classes.icon}
            />
            </ScrollUp>
        </div>
    );
}
 
export default Home;