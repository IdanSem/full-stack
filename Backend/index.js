const express = require('express')
const app = express()
const mysql = require('mysql')
const cors = require('cors')
const { default: axios } = require('axios')

const db = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "idanpro100",
    database: 'rafealdb',
    port: 3306
})

app.use(cors())
app.use(express.json())

app.get('/idanApi/getData', (req, res) => {
        db.query("SELECT * FROM attacks", (err, data) => {
            if(err) throw(err)
            res.send(data)
        })
})

app.post('/support/idanApi/suppConversion', (req, res) => {
    userMsg = req.body.msg
    checkerCommand = userMsg.split('&')
    // protocol language
    if(checkerCommand[0] === 'CODE1' && checkerCommand.length === 3) {
        sqlQueryGetColumns = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_NAME`='attacks';"
        db.query(sqlQueryGetColumns, (err, keys) => {
            if(err) throw(err)
            let key = checkerCommand[1].toLowerCase()
            let keyWords = checkerCommand[2]
            if(!validKey(keys, key)) {
                res.send('Not valid key, use "/help keys"')
            }
            else {
                db.query(`SELECT * FROM attacks where (${key} LIKE ?)`, [('%' + keyWords + '%')], (err, data) => {
                    if(err) throw(err)
                    res.send(data)
                })
            }
        })
    } else if(checkerCommand[0] === 'CODE2' && checkerCommand[1].toLowerCase() === 'check the hash') {
        // if(!checkerCommand[2].match('^[A-Za-z0-9]+$')) {
        //     res.send('Hash can made up of letters and numbers - ONLY')
        // }
        // else {
            axios({
                method: 'post',
                url: 'https://www.virustotal.com/vtapi/v2/file/report',
                params: {
                    'apikey': '8bef90cd0adb9d4b14152540462bb9aec9dd0ea49a9d846836673a450baaae36',
                    'resource': checkerCommand[2]
                }
            })
            .then(({data}) => {
                let viruses = []
                Object.keys(data.scans).forEach(key => {
                    if(data.scans[key]['detected']) viruses.push(key)
                })
                res.send(viruses.length ? ("Names of virus" + (viruses.length !== 1 ? 'es' : '') + ` detected:\n ${viruses.join(', ')}.\n\nFound (${viruses.length}/${Object.keys(data.scans).length}) engines detected in this hash`) : "The hash file is completely clear.")
            })
            .catch(err => {
                res.send("Hash invalid, check your's hash...\nReminder: Accepting only md5, sha1, sha256.")
            })
        // }
    } else {
        if(userMsg.toLowerCase().includes('hey')) {
            res.send('Hello there, for my commands enter "/help"')
        } else if(userMsg.toLowerCase().includes('hello')) {
            res.send('Hey buddy, for my commands enter "/help"')
        } else if(userMsg.toLowerCase().includes('how are you')) {
            res.send('Im fine, what about you?')
        } else if(userMsg.toLowerCase() === '/help') {
            res.send('My commands:\nFirst, i can search for information about attacks.\nFormat is: CODE1&<input key>&<input keywords>\nFor keys: /help keys.\nSecond, i can scan hashes (md5, sha1, sha256).\nFormat is: CODE2&Check the hash&<your hash>\n\nExamples:\nCODE1&description&dns\nCODE2&Check the hash&d41d8cd98f00b204e9800998ecf8427e')
        } else if(userMsg.toLowerCase() === '/help keys') {
            sqlQueryGetColumns = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_NAME`='attacks';"
            db.query(sqlQueryGetColumns, (err, keys) => {
                if(err) throw(err)
                let returnMsg = 'Keys: ' + keys.map(key => key['COLUMN_NAME']).join(', ') + '.'
                res.send(returnMsg)
            })
        } else {
            res.send(`I didn't understand... What do you mean?\n(For my commands enter "/help").`)
        }
    }
})

app.listen(3001, () => {
 console.log(`running on port 3001`)
})

const validKey = (keys, key) => {
    let flag = false 
    keys.forEach(dict => {
        if(dict['COLUMN_NAME'] === key && !flag) {
            flag = true
        }
    })
    return flag
}