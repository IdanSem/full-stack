import React from 'react';
import Navbar from './Navbar'
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline"
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './Home'
import Chatbot from './Chatbot';

const themeDark = createMuiTheme({
  palette: {
    background: {
      default: "#222222"
    },
    text: {
      secondary: "#3f51b5"
    },
  },
  typography: {
    h3: {
      color: 'black'
    }
  },
})


function App() {

    return (
        <MuiThemeProvider theme={themeDark}>
            <CssBaseline />
              <Router>
              <Navbar/>
              <Switch>
                <Route exact path="/">
                  <Home/>
                </Route>
                <Route path="/support">
                  <Chatbot/>
                </Route>
              </Switch> 
            </Router>
        </MuiThemeProvider>
    );

}
export default App