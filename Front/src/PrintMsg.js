import React, { useEffect, useRef, useState } from 'react';
import { Box, makeStyles, Typography, Button} from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

const useStyles = makeStyles({
    msgFrameSent: {
        margin: '1% 1% 0px 0px',
        background: '#367FD2',
        color: 'white',
        borderRadius: 10,
        padding: '2px 8px 2px 8px',
        float: 'right',
        maxWidth: '65%',
        height: 'auto'
    },
    msgFrameRecv: {
        margin: '1% 0px 0px 1%',
        background: '#cecece',
        borderRadius: 10,
        padding: '2px 8px 2px 8px',
        float: 'left',
        maxWidth: '65%',
        height: 'auto'
    },
    text: {
        fontSize: 20,
        padding: 4,
        overflowWrap: 'break-word',
        whiteSpace: 'pre-line',
    },
    userIcon: {
        margin: '4px 4px 0px 0px',
        float: 'right',
    },
    serverIcon: {
        margin: '4px 0px 0px 4px',
        float: 'left',
    },
    scroller: {
        position: 'relative',
        scrollBehavior: 'smooth',
        msOverflowStyle: 'none',
        scrollbarWidth: 'none', 
        overflowY: 'scroll',
    },
    buttonBottom: {
        position: 'absolute',
        color: 'white',
        background: 'black',
        borderRadius: 0,
        right: 0,
        bottom: 0,
        marginBottom: '5%',
    },
    allWidth: {
        flexBasis: '100%',
    }
})

const PrintMsg = ({allMsg}) => {

    const classes = useStyles()
    let wholeChatRef = useRef(null)

    const [biggestHeight, setBiggestHeight] = useState(0)
    const [nowHeight, setNowHeight] = useState(0)

    useEffect(() => {
        scrollToBottom()
        setBiggestHeight(wholeChatRef.current.scrollHeight - wholeChatRef.current.clientHeight)
    }, [allMsg])

    const scrollHandler = () => {
        setNowHeight(wholeChatRef.current.scrollTop)            
    }

    const scrollToBottom = (e) => {
            wholeChatRef.current.scrollTop = wholeChatRef.current.scrollHeight - wholeChatRef.current.clientHeight;
    } 

    return (
        <>
            <Box className={classes.scroller} width={1} id='allMsg' onScroll={scrollHandler} ref={wholeChatRef}>
                {
                    allMsg.map((msg, i) => (
                        <Box
                        key={i}
                        display='flex'
                        >
                            <Box justifyContent={(msg.sentFrom === 'user') ? 'flex-end' : 'flex-start'} className={classes.allWidth}>
                                {
                                    msg.sentFrom === 'user'
                                    ? <img src='userIcon.png' alt='' width="60" height="60" className={classes.userIcon}></img>
                                    : <img src='serverIcon.png' alt='' width="60" height="60" className={classes.serverIcon}></img>
                                }
                                <Box className={ (msg.sentFrom === 'user') ? classes.msgFrameSent : classes.msgFrameRecv}>
                                    <Typography className={classes.text}>
                                        {msg.data}
                                    </Typography>
                                </Box>
                            </Box>  
                        </Box>   
                    ))
                }
                <p id="demo"></p>
            </Box>
            {(biggestHeight - 120 > nowHeight) ? 
            <Button className={classes.buttonBottom} onClick={scrollToBottom}>
                <ArrowDownwardIcon></ArrowDownwardIcon>
            </Button>
            :
            <></>
            }
        </>    
    )
}
 
export default PrintMsg;