const mysql = require('mysql')
const fs = require('fs')

let path = './data/'

const db = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "idanpro100",
    database: 'rafealdb',
    port: 3306
})

const files = fs.readdirSync(path) // get files

files.forEach(file => {
    fs.readFile(path + file, 'utf8', (err, data) => {
        if(err) throw(err)
        data = JSON.parse(data)['objects'][0] // our object
        let phase_names = []
        if(data['name'] == undefined) data['name'] = 'Na'
        if(data['description'] == undefined) data['description'] = 'Na'
        if(data['id'] == undefined) data['id'] = 'Na'
        if(data['x_mitre_platforms'] == undefined) data['x_mitre_platforms'] = 'Na'
        else data['x_mitre_platforms'] = data['x_mitre_platforms'].join(' ---> ')
        if(data['x_mitre_detection'] == undefined) data['x_mitre_detection'] = 'Na'
        if(data['kill_chain_phases'] == undefined) phase_names = 'Na'
        else {
            phase_names = data['kill_chain_phases'].map(({phase_name}) => phase_name).join(' ---> ')
        } 
        const insertAttackCommand = "INSERT INTO attacks values(?, ?, ?, ?, ?, ?)";
        db.query(insertAttackCommand, [data.name, data.id, data.description, data.x_mitre_platforms, data.x_mitre_detection, phase_names],(err) => {
            if(err) {
                throw(err)
            }
            // console.log(file + " added to attacks")
        });
    })
})
