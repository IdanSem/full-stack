import React, { useState } from 'react';
import { Button, Box, makeStyles, Typography, TextField } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import './chatBot.css'
import Axios from 'axios'
import PrintMsg from './PrintMsg'

const useStyles = makeStyles({
    wholeFrame: {
        marginTop: '1.7%',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '80vw',
        height: '80vh',
        borderRadius: 5,
        flexDirection:'column',
        flexWrap:'nowrap',
        overflow:'hidden'
    },
    frame: {
        flexGrow: 1,
        flexShrink: 1,
        overflow:'hidden',
        position:'relative',
        background: 'white',
        borderLeft: '3px solid black',
        borderRight: '3px solid black',
    },
    myTheme: {
        fontSize: 20,
        marginTop: 12,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'Copperplate Gothic Light',
        marginLeft: 'auto',
    },
    robotIcon: {
        marginLeft: 8,
        marginTop: 4,
        paddingBottom: 3,
        marginRight: 'auto',
    },
    borderTheme: {
        flexBasis: '100%',
        display: 'flex',
        background: 'black',
    },
    gridInput: {
        background: 'black',
        padding: 5,
    },
    input: {
        margin: '4px 0px 4px 10px',
        padding: '0px 10px 4px 10px',
        width: '90%',
        background: '#495057',
        borderRadius: 15,
    },
    inputC: {
        color: 'white',
    },
    buttomDisplay: {
        marginTop: 2,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
})

const Chatbot = () => {

    const classes = useStyles()
    const [userMsg, setUserMsg] = useState('')
    const [allMsg, setMoreMsg] = useState([])

    function handleSend() {
        if(userMsg !== '') {
            document.getElementById('textField').value='' // clear
            
            var objDiv = document.getElementById("allMsgs");
            objDiv.scrollTop = objDiv.scrollHeight;
            
            setUserMsg('') // clear
            setMoreMsg(allMsg => [...allMsg, objMsg(userMsg, 'user')])
            Axios.post('http://localhost:3001/support/idanApi/suppConversion', 
                {msg: userMsg}
            )
            .then(({data}) => { 
                if(typeof data === typeof {}) {
                    let logAttacks = data.length ? `Found ${data.length} results:\n\n` : "No attacks found."
                    data.forEach((attack, index) => {
                        logAttacks += `Attack ${index + 1}:\nAttack ID is "${attack.id}"\nAttack name is "${attack.name}"\n\n`
                    })
                    logAttacks += 'For more information you can search in Home'
                    setMoreMsg(allMsg => [...allMsg, objMsg(logAttacks, 'server')])
                } else {
                setMoreMsg(allMsg => [...allMsg, objMsg(data, 'server')])
                }
            })
            .catch(err => {
                console.log(err)
            })
        }
    }

    const handleEnter = (event) => {
        if(event.keyCode === 13) {
            handleSend()
        }
    }

    return ( 
        <Box className={classes.wholeFrame} display='flex'>
            <Box display='flex' flexShrink={1} flexGrow={0}>
                <div className={classes.borderTheme}>
                    <Typography className={classes.myTheme}>
                        Chat Bot 
                    </Typography>
                    <img src="robotIcon.png" alt="" width="38.23" height="50" className={classes.robotIcon}></img>
                </div>
            </Box>
            <Box display='flex' flexShrink={0} flexGrow={1}  className={classes.frame} id='allMsgs'>
                {(allMsg.length > 0) && <PrintMsg allMsg={allMsg}/>}                        
            </Box>
            <Box display='flex' flexShrink={1} flexGrow={0} className={classes.gridInput}>
                
                    <TextField 
                    className={classes.input}
                    onChange={changeTextHandle}
                    id='textField'
                    autoComplete='off'
                    onKeyDown={handleEnter}
                    InputProps={
                        {
                            classes: {
                                input: classes.inputC
                            },
                        }
                    }
                    > 
                    </TextField>
                    <Button className={classes.buttomDisplay} onClick={handleSend}>
                        <SendIcon className={classes.sendIcon} color='primary'></SendIcon>
                    </Button>
               
            </Box>
        </Box>
    );

    function changeTextHandle(e) {
        setUserMsg(e.target.value)
    }

    function objMsg(msg, userOrServer) {
        return {
            data: msg,
            sentFrom: userOrServer
        }
    }
}

export default Chatbot;
